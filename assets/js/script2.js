/* =====================================================
	WD005-S2-DOM-MANIPULATION-2
	GitLab: s2-js-dom-man-2

	Manipulate the document object to add tasks one at a time and delete all of them at once.

	STRETCH GOAL:
  Delete individual tasks.
  
  CLUE: Use data attributes to store unique values to each new Task added.

	STRESS GOAL:
	Update individual tasks.
===================================================== */


const wonderWoman = document.querySelector('ul#roster li:last-child');
wonderWoman.addEventListener('click', function(){
	wonderWoman.classList.toggle('underline');
})

//d) CHECK IF AN ELEMENT HAS A PARTICULAR CLASS
console.log(wonderWoman.classList.contains('hero'));
console.log(wonderWoman.classList.contains('villain'));



console.log('*********** GETTING VALUES FROM INPUT FIELDS *************');
/*======================
5) GETTING VALUES FROM INPUT FIELDS
=======================*/

document.querySelector("#btn-print").addEventListener('click', function(e){
	alert('hello');
	console.log(e);
	console.log(e.type);
	console.log(e.target);
	let element = e.target;
	console.log(document.querySelector("#input"));



	let msg = document.querySelector("#input");
	console.log(msg.value);
	//alert(msg.value);


	// let msgLength = 0;
	// const textContainerVar = document.querySelector('#text-container');
	// while(msg){
	// 	if(msg){
	// 		document.querySelector('#text-container').textContent = msg.value;
	// 		msgLength++;
	// 	}
	// 	document.querySelector('#input').value = '';
	// }



/*
	let section = document.querySelector("section");		//no pound sign because 'section' is an element/tag
	let p = document.createElement('p');				//paragraph element <p> tag
	console.log(p);
	p.textContent = "This is a sample appended paragraph.";
	console.log(p);
	section.appendChild(p);		//dynamically add the paragraph to the webpage (not in index.html)
	//console.log(section);		//append: // sa baba - before ng end tag <section>
*/
	
	let list = document.querySelector('#text-container');
	let li = document.createElement('li');
	console.log(li);
	li.textContent = msg.value;
	console.log(li.textContent);
	list.appendChild(li);
	document.querySelector('#input').value = '';	//

	// document.querySelector('#text-container').textContent = msg.value;	//add the entered value to the html element 'textContent'
	// document.querySelector('#input').value = '';	//


	// console.log("****** show text-container object below ******");
	// console.log(document.querySelector('#text-container'));


	// const textContainerVar = document.querySelector('#text-container');
	// const colVar = document.querySelector('.col');
	// console.log(textContainerVar.childNodes);
	// console.log("textContainerVar.childNodes[0].data");
	// console.log(textContainerVar.childNodes[0].data);
	// console.log(colVar);


	//console.log(document.querySelector('#text-container').textContent.childNodes[1].data);
});



