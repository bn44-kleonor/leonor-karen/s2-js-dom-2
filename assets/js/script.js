/*======================
WD005-S2-DOM MANIPULATION

REVIEW: DOCUMENT OBJECT MODEL (DOM)
- a ___________________________ by the browser that enables the HTML structure to 
be easily accessed by style sheets and programming languages.

- ____________ network of nodes: 

- to the DOM, everything on a web page is a ________

WE COVERED:
1) LEGACY DOM SHORTCUT METHODS
2) NODE LISTS
3) METHODS TO ACCESS ELEMENTS
4) CREATE AND PLACE NEW NODES
5) MOVE AND REMOVE NODES
6) MANIPULATING STYLES
7) DOM EVENTS

code along and notes  Ms Joyce (instructor)
===========================*/
console.log('*********** DOM NAVIGATION *************');
//8) DOM NAVIGATION
/*node objects have a number of ___properties___ and ____methods____ for 
navigating the document tree*/
const heroes = document.querySelector("#roster");		// "#" means accessing "id" in html file
console.log(`>>>>>>>> heroes = document.querySelector("#roster")`);
console.log(heroes);
console.log(" ");

//a) childNodes - text and _______ are treated as text nodes
console.log(">>>>>>>> heroes.childNodes");
console.log(heroes.childNodes);			//in reference to 'roster' id
console.log(" ");


//b) children - no text nodes
console.log(">>>>>>>> heroes.children");
console.log(heroes.children);			//in reference to 'roster' id
console.log(" ");


//c) firstChild - returns the first child of a node
console.log(">>>>>>>> heroes.firstChild");
console.log(heroes.firstChild);			//in reference to 'roster' id
console.log(" ");


//d) lastChild - returns the last child of a node
console.log(">>>>>>>> heroes.lastChild");
console.log(heroes.lastChild);			//in reference to 'roster' id
console.log(" ");

//e) parentNode - returns the parent node of an element
console.log(">>>>>>>> heroes.parentNode");
console.log(heroes.parentNode);			//in reference to 'roster' id
console.log(" ");


const wonderWoman = document.querySelector('ul#roster li:last-child');
console.log(`>>>>>>>> wonderWoman = document.querySelector('ul#roster li:last-child')`);
console.log(">>>>>>>> wonderWoman");
console.log(wonderWoman);
console.log(" ");

console.log(">>>>>>>> wonderWoman.parentNode");
console.log(wonderWoman.parentNode);
console.log(" ");

console.log(">>>>>>>> wonderWoman.parentNode.parentNode");
console.log(wonderWoman.parentNode.parentNode);
console.log(" ");

//f) nextSibling - returns the next adjacent node of the same parent
console.log(">>>>>>>> wonderWoman.nextSibling");
console.log(wonderWoman.nextSibling);
console.log(" ");


//g) previousSibling - returns the next adjacent node of the same parent
console.log(">>>>>>>> wonderWoman.previousSibling");
console.log(wonderWoman.previousSibling);
console.log(" ");

/*again, childNodes, firstChild, lastChild, nextSibling, and previousSibling propreties 
return ___nodes___, NOT just the elements*/



console.log('*********** FINDING THE VALUE OF A NODE *************');
/*======================
2) FINDING THE VALUE OF A NODE
=======================*/
console.log(">>>>>>>> wonderWoman");
console.log(wonderWoman);
console.log(" ");

console.log(`>>>>>>>> text = wonderWoman.firstChild`);
const text = wonderWoman.firstChild;
console.log(`>>>>>>>> text`);
console.log(text);
console.log(" ");


//a) nodeValue
console.log(`>>>>>>>> text.nodeName`);
console.log(text.nodeName);		// #text
console.log(" ");

console.log(`>>>>>>>> text.nodeType`);
console.log(text.nodeType);		// 1=element, 2=attribute, 3=text, 8=comment, 9=body
console.log(" ");

console.log(`>>>>>>>> text.nodeValue`);
console.log(text.nodeValue);
console.log(" ");


//b) textContent
console.log(`>>>>>>>> wonderWoman.textContent`);
console.log(wonderWoman.textContent);
console.log(" ");


console.log('*********** GETTING, SETTING, CHECKING, AND REMOVING ATTRIBUTES *************');
/*======================
3) GETTING, SETTING, CHECKING, AND REMOVING ATTRIBUTES
- all HTML elements have a large number of possible attributes
- the DOM has ___setters___and ___getters___ methods to view, add, remove, or 
modify the value of any of these attributes
=======================*/

//a) GETTING AN ELEMENT's ATTRIBUTES
console.log(`>>>>>>>> wonderWoman.getAttribute('class')`);
console.log(wonderWoman.getAttribute('class'));
console.log(" ");

console.log(`>>>>>>>> wonderWoman.getAttribute('src')`);
console.log(wonderWoman.getAttribute('src'));		//if the Attribute does not exist result is 'null'.
console.log(" ");


//b) SETTING AN ELEMENT's ATTRIBUTES
//the setAttribute() method changes the ___value___ of an element's attributes
//it takes the ff. 2 arguments:
//the attribute that we wish to change
//the new value of that attribute
console.log(`>>>>>>>> wonderWoman.setAttribute('class','villain')`);
wonderWoman.setAttribute('class','villain');	//changed the 'class' name 'hero' into 'villain'
console.log(wonderWoman.getAttribute('class'));
console.log(`comment-->>>>>> changed the 'class' name 'hero' into 'villain'`);
console.log(" ");


//if an element does not have an attribute, the setAttribute 
//method will add it to the element
console.log(`>>>>>>>> wonderWoman.setAttribute('id', 'amazon')`);
wonderWoman.setAttribute('id', 'amazon');			//originally walang 'id' pero kapag ni-set magkakaroon ng id
console.log(" ");

console.log(`>>>>>>>> wonderWoman.getAttribute('id')`);
console.log(wonderWoman.getAttribute('id'));
console.log(" ");

//c) CHECKING AN ELEMENT's ATTRIBUTES
console.log(`>>>>>>>> wonderWoman.hasAttribute('id')`);
console.log(wonderWoman.hasAttribute('id'));
console.log(" ");

console.log(`>>>>>>>> wonderWoman.hasAttribute('value')`);
console.log(wonderWoman.hasAttribute('value'));
console.log(" ");


//d) REMOVING AN ELEMENT'S ATTRIBUTE
console.log(`>>>>>>>> wonderWoman.removeAttribute('id')`);
wonderWoman.removeAttribute('id');
console.log(`>>>>>>>> wonderWoman.hasAttribute('id')`);
console.log(wonderWoman.hasAttribute('id'));	//dapat false na kasi ni-delete na
console.log(`comment-->>>>>>>> 'false' kasi na delete/removed na yung 'id'`)
console.log(" ");


console.log('*********** ADDING, REMOVING, & TOGGLING CLASSES OF AN ELEMENT *************');
/*======================
4) ADDING, REMOVING, & TOGGLING CLASSES OF AN ELEMENT
- the className property lets us set the class of an element ___directly___
- it also lets us find out the value of its class attribute
=======================*/
//a) FIND OUT THE VALUE OF CLASS ATTRIBUTE
console.log(`//a) FIND OUT THE VALUE OF CLASS ATTRIBUTE (using className)`);
console.log(`>>>>>>>> wonderWoman.className`);
console.log(wonderWoman.className);
console.log(" ");

//b) DIRECTLY SET THE VALUE OF A CLASS ATTRIBUTE
console.log(`//b) DIRECTLY SET THE VALUE OF A CLASS ATTRIBUTE (using className)`);
console.log(`>>>>>>>> wonderWoman.className = 'hero'`);
wonderWoman.className = 'hero';			//changed the class name form 'villain' to 'hero'
console.log(`>>>>>>>> wonderWoman.className`);
console.log(wonderWoman.className);
console.log(" ");


/* 
    CAUTION 
    - changing the className property of an element by assignment 
    will ____overwrite____all other classes that have been set on the element
    to avoid this problem, use the classList property instead
*/

//a.2) FIND OUT THE VALUE OF CLASS ATTRIBUTE
console.log(`//a.2) FIND OUT THE VALUE OF CLASS ATTRIBUTE (using classList)`);
console.log(`>>>>>>>> wonderWoman.classList`);
console.log(wonderWoman.classList);
console.log(" ");

console.log(`>>>>>>>> wonderWoman.classList.value`);
console.log(wonderWoman.classList.value);
console.log(" ");


//b.2) SET THE VALUE OF A CLASS ATTRIBUTE
console.log(`//b.2) SET THE VALUE OF A CLASS ATTRIBUTE`);
console.log(`>>>>>>>> wonderWoman.classList.add('goddess')`);
wonderWoman.classList.add('goddess');		//add a class to existing classes
console.log(`>>>>>>>> wonderWoman.classList`);
console.log(wonderWoman.classList);
console.log(" ");
console.log(`>>>>>>>> wonderWoman.classList.value`);
console.log(wonderWoman.classList.value);	//returns the values of 'class' attribute
console.log(" ");
console.log(`>>>>>>>> wonderWoman.className`);
console.log(wonderWoman.className);			//returns the values of 'class' attribute
console.log(" ");



/* 
    the classList property is a list of all the class that an element has
    it has an add() method to add a class to an element WITHOUT ___overwriting___
    any classes that it already has
*/


//c) ADD AND REMOVE A CLASS ATTRIBUTE
//the toggle() method adds a class if the passed in class argument does not exist yet 
//and removes it if it the target element already has it
//returns true if the class was added, false if it was removed
console.log(`>>>>>>>> wonderWoman.classList.toggle('hero')`);
console.log(wonderWoman.classList.toggle('hero'));		// false
console.log(`comment-->>>>>>>> "false" because it is already existing so toggle did NOT add it`);
console.log(`>>>>>>>> wonderWoman.classList`);
console.log(wonderWoman.classList);						// false because it is already existing so toggle did not add it
console.log(" ");
console.log(`>>>>>>>> wonderWoman.classList.toggle('hero')`);
console.log(wonderWoman.classList.toggle('hero'));		// true
console.log(`comment-->>>>>>>> "true" because it is not yet existing so toggle ADDED it`);
console.log(`>>>>>>>> wonderWoman.classList`);
console.log(wonderWoman.classList);						// true because it is not yet existing so toggle added it
console.log(" ");

//const wonderWoman = document.querySelector('ul#roster li:last-child');
wonderWoman.addEventListener('click', function(){
	wonderWoman.classList.toggle('underline');
})

//d) CHECK IF AN ELEMENT HAS A PARTICULAR CLASS
console.log(`//d) CHECK IF AN ELEMENT HAS A PARTICULAR CLASS`);
console.log(`>>>>>>>> wonderWoman.classList.contains('hero')`);
console.log(wonderWoman.classList.contains('hero'));
console.log(" ");
console.log(`>>>>>>>> wonderWoman.classList.contains('villain')`);
console.log(wonderWoman.classList.contains('villain'));
console.log(" ");


console.log('*********** GETTING VALUES FROM INPUT FIELDS *************');
/*======================
5) GETTING VALUES FROM INPUT FIELDS
=======================*/

document.querySelector("#btn-print").addEventListener('click', function(e){
	//alert('hello');
	console.log(`>>>>>>>> log(e)`);
	console.log(e);
	console.log(" ");
	console.log(`>>>>>>>> log(e.type)`);
	console.log(e.type);
	console.log(" ");
	console.log(`>>>>>>>> log(e.target)`);
	console.log(e.target);
	console.log(" ");
	let element = e.target;
	console.log(`>>>>>>>> document.querySelector("#input")`);
	console.log(document.querySelector("#input"));
	console.log(" ");

	let msg = document.querySelector("#input");
	console.log(`>>>>>>>> let msg = document.querySelector("#input")`);
	console.log(`>>>>>>>> msg.value`);
	console.log(msg.value);
	console.log(" ");
	//alert(msg.value);
	console.log(`>>>>>>>> document.querySelector('#text-container').textContent = msg.value;`)
	console.log(`means: to add the entered value to the html element id object #text-container under its property 'textContent'`)
	document.querySelector('#text-container').textContent = msg.value;	//add the entered value to the html element 'textContent'
	document.querySelector('#input').value = '';	//
});
